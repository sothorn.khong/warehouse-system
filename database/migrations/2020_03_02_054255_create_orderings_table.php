<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderings', function (Blueprint $table) {
            $table->increments('id');
            $table->date("RequireDate")->nullable();
            $table->string("RealNum")->nullable();
            $table->string("RealSeq")->nullable();
            $table->string("SaleNum")->nullable();
            $table->integer("Seq")->nullable();
            $table->date("SaleDate")->nullable();
            $table->string("MemberId")->nullable();
            $table->string("MemberName")->nullable();
            $table->string("ItemNo")->nullable();
            $table->string("ItemName")->nullable();
            $table->integer("Quantity")->nullable();
            $table->string("S_ItemNo")->nullable();
            $table->string("S_ItemName")->nullable();
            $table->integer("S_Quantity")->nullable();
            $table->string("Unit")->nullable();
            $table->string("DeliName")->nullable();
            $table->string("PhoneNo")->nullable();
            $table->string("MobileNo")->nullable();
            $table->text("Address1")->nullable();
            $table->text("Address2")->nullable();
            $table->string("Postcode")->nullable();
            $table->string("DeiCity")->nullable();
            $table->string("DeliState")->nullable();
            $table->string("Attention")->nullable();
            $table->date("DeliDate")->nullable();
            $table->string("TagSMS")->nullable();
            $table->string("SongNum")->nullable();
            $table->time("DeliveryTime")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderings');
    }
}
