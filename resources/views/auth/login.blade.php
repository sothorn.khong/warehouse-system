@extends('layouts.app')

@section('content')

<!-- 
<div class="container">
    <div class="row"> -->

    <div class="pad-t-70">

        <div class="col-md-4 col-md-offset-4">
            <form method="POST" action="{{ route('login') }}" id="login-form" class="smart-form client-form form-box-shadow">
                {{ csrf_field() }}
                <fieldset class="pad-tb-50 pad-lr-20">
                    <section>
                        <center>
                            <img src="{{asset('img/logo.jpg')}}" class="img-responsive w-150"/>
                        </center>
                    </section>
                    <section>
                        <label class="label"><b>E-mail</b></label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                    <span class="text-danger">
                                        {{ $errors->first('email') }}
                                    </span>
                            @endif
                    </section>
                    <section >
                        <label class="label"><b>Password</b></label>
                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                            <input id="password" type="password" name="password" required>
                            @if ($errors->has('password'))
                                <span class="text-danger">
                                    {{ $errors->first('password') }}
                                </span>
                            @endif
                    </section>
                    <section>
                        <div class="row">
                                <div class="col col-6">
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <i></i> Remember Me
                                    </label>
                                </div>
                                <div class="col col-6">
                                    <div class="note pull-right">
                                        <a href="{{ route('password.request') }}">Forgot password?</a>
                                    </div>
                                </div>
                        </div>     
                    </section>
                    <hr/>
                    <section>
                        <button type="submit" class="btn btn-bg-black w-full pad-tb-7">
                            Sign In
                        </button>
                    </section>
                </fieldset>
            </form>
        </div>
    </div>
        <!-- <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    <!-- </div>
</div> -->


@endsection
