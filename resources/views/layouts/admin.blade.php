<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="warehouse">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" media="screen" href="{{asset('css/font-awesome.min.css')}}">
	<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
	<link rel="stylesheet" type="text/css" media="screen" href="{{asset('css/smartadmin-production.min.css')}}">
	<link rel="stylesheet" type="text/css" media="screen" href="{{asset('css/smartadmin-skins.min.css')}}">
	<!-- SmartAdmin RTL Support -->
	<link rel="stylesheet" type="text/css" media="screen" href="{{asset('css/smartadmin-rtl.min.css')}}"> 
	
	<link rel="stylesheet" type="text/css" media="screen" href="{{asset('app/asset/toast/angular-toastr.css')}}"> 
	

</head>
<body class="" ng-controller="mainCtr">
		<!-- #HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo"> <img src="{{asset('img/logo.jpg')}}" alt="Enevo"> </span>
				<!-- END LOGO PLACEHOLDER -->
			</div>
			<!-- #TOGGLE LAYOUT BUTTONS -->

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				
				<!-- #MOBILE -->
				<!-- Top menu profile link : this shows only when top menu is active -->
				<ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
					<li class="">
						<a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown"> 
							<img src="img/avatars/sunny.png" alt="John Doe" class="online" />  
						</a>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="#ajax/profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="login.html" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
							</li>
						</ul>
					</li>
				</ul>

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="{{ route('logout') }}" title="Sign Out"  ><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

				<!-- multiple lang dropdown : find all flags in the flags page -->
				<ul class="header-dropdown-list hidden-xs">
					<li>
						<a href class="dropdown-toggle" data-toggle="dropdown"> <img src="img/blank.gif" class="flag flag-us" alt="United States"> <span> US</span> <i class="fa fa-angle-down"></i> </a>
						<ul class="dropdown-menu pull-right">
							<li >
								<a href ng-click="setLang_set('en')"><img src="img/blank.gif" class="flag flag-us" alt="United States"> English (US)</a>
							</li>
							<li>
								<a  href ng-click="setLang_set('kh')"><img src="img/blank.gif" class="flag flag-kh" alt="Khmer"> Khmer</a>
							</li>

							<!-- <li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-es" alt="Spanish"> Español</a>
							</li>
							<li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-de" alt="German"> Deutsch</a>
							</li>
							<li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-jp" alt="Japan"> 日本語</a>
							</li>
							<li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-cn" alt="China"> 中文</a>
							</li>	
							<li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-it" alt="Italy"> Italiano</a>
							</li>	
							<li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-pt" alt="Portugal"> Portugal</a>
							</li>
							<li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-ru" alt="Russia"> Русский язык</a>
							</li>
							<li>
								<a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-kr" alt="Korea"> 한국어</a>
							</li>						 -->
						</ul>
					</li>
				</ul>
				<!-- end multiple lang -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- #NAVIGATION -->
		<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS/SASS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="img/avatars/sunny.png" alt="me" class="online" /> 
						<span>
							john.doe 
						</span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive

			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->
			<nav >
				<!-- 
				NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

				<ul>
					<li class="">
						<a href="#!home" title="blank_"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">@{{LANG.ORDERING}}</span></a>
					</li>
					<li class="">
						<a href="#!delivery" title="blank_"><i class="fa fa-lg fa-fw fa-truck"></i> <span class="menu-item-parent">@{{LANG.DELIVERY}}</span></a>
					</li>
					<li class="">
						<a href="#!warehouse" title="blank_"><i class="fa fa-lg fa-fw fa-cubes"></i> <span class="menu-item-parent">@{{LANG.WAREHOUSE}}</span></a>
					</li>
					<li class="">
						<a href="#!product" title="blank_"><i class="fa fa-lg fa-fw fa-product-hunt"></i> <span class="menu-item-parent">@{{LANG.PRODUCT}}</span></a>
					</li>
					<li class="">
						<a href="#!brand" title="blank_"><i class="fa fa-lg fa-fw fa-map-marker"></i> <span class="menu-item-parent">@{{LANG.BRAND}}</span></a>
					</li>
					<li class="">
						<a href="#!category" title="blank_"><i class="fa fa-lg fa-fw fa-cube"></i> <span class="menu-item-parent">@{{LANG.CATEGORY}}</span></a>
					</li>
					<li class="">
						<a href="#!customer" title="blank_"><i class="fa fa-lg fa-fw fa-users"></i> <span class="menu-item-parent">@{{LANG.CUSTOMER}}</span></a>
					</li>
					<li class="">
						<a href="#!user" title="blank_"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">@{{LANG.USER}}</span></a>
					</li>
					<!-- <li class="">
						<a href="#!group" title="blank_"><i class="fa fa-lg fa-fw fa-user-secret"></i> <span class="menu-item-parent">@{{LANG.USER_GROUP}}</span></a>
					</li> -->
					<li class="">
						<a href="#!setting" title="blank_"><i class="fa fa-lg fa-fw fa-cog"></i> <span class="menu-item-parent">@{{LANG.SETTING}}</span></a>
					</li>
					<li class="">
						<a href="#!report" title="blank_"><i class="fa fa-lg fa-fw fa-bars"></i> <span class="menu-item-parent">@{{LANG.REPORT}}</span></a>
					</li>
				</ul>
			</nav>

			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>
		<!-- END NAVIGATION -->
		
		<!-- #MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			
			<!-- END RIBBON -->

			<!-- #MAIN CONTENT -->

            @yield('content')
    
			<!-- end row -->
			<!-- END #MAIN CONTENT -->

		</div>
		<!-- END #MAIN PANEL -->

		<!-- #PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">Enevo<span class="hidden-xs"> - Web Application</span> © 2020-2021</span>
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- END FOOTER -->

		<!-- #SHORTCUT AREA : With large tiles (activated via clicking user name tag)
			 Note: These tiles are completely responsive, you can add as many as you like -->
		<div id="shortcut">
			<ul>
				<li>
					<a href class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
				</li>
			</ul>
		</div>

		

	
		<script src="{{asset('js/libs/jquery-3.2.1.min.js')}}"></script>
		<script src="{{asset('js/libs/jquery-ui.min.js')}}"></script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="{{asset('js/app.config.js')}}"></script>
		<!-- BOOTSTRAP JS -->
		<script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
		<script src="{{asset('js/app.min.js')}}"></script>

		<script src="{{asset('app/asset/angular1_7.min.js')}}"></script>
		<script src="{{asset('app/asset/angular-route.min.js')}}"></script>
		<script src="{{asset('app/asset/ocLazyLoad.min.js')}}"></script>
		<script src="{{asset('app/asset/toast/angular-animate.min.js')}}"></script>
		<script src="{{asset('app/asset/toast/angular-toastr.tpls.js')}}"></script>
		<script src="{{asset('app/asset/upload/angular-file-upload.js')}}"></script>


		
		<!--[if IE 8]>
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
		<![endif]-->

		<!-- MAIN APP JS FILE -->

		

		<script>
			//var warehouseApp = angular.module("warehouse",[ "ngRoute", "oc.lazyLoad"]);
			var warehouseApp = angular.module('warehouse', [ "ngRoute", "oc.lazyLoad","ngAnimate", "toastr","angularFileUpload"]);
			var lang_name = "en";
			var LANG = {};
			warehouseApp.controller("mainCtr",function($scope,$ocLazyLoad,$window){

				if(sessionStorage.getItem("lang")){
					$scope.lang_session = sessionStorage.getItem("lang");
				}else{
					$scope.lang_session = lang_name;
				}

				$scope.setLang = function(value){
					sessionStorage.setItem("lang", value);
					$ocLazyLoad.load('app/setting/lang/'+value+'.js');
					$scope.LANG = LANG;
					
				}

				$scope.setLang_set = function(value){
					sessionStorage.setItem("lang", value);
					$window.location.reload();
					
				}
				$scope.setLang($scope.lang_session);
			});
		</script>

		<script src="{{asset('app/setting/route.js')}}"></script>
		<script src="{{asset('app/setting/httpClient.js')}}"></script>
		<script src="{{asset('app/asset/directive/pagination.js')}}"></script>
		
		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script>
	
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>

	
   
</body>
</html>