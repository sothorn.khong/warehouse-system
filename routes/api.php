<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource("setting","SettingController");
Route::apiResource("users","UserController");
Route::apiResource("brand","BrandController");
Route::apiResource("category","CagetoryController");
Route::apiResource("customer","CustomerController");
Route::apiResource("product","ProductController");
Route::apiResource("ordering","OrderingController");