<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout');
Route::post('/search_user', 'UserController@search_user');
Route::post('/search_brand', 'BrandController@search_brand');
Route::post('/search_category', 'CagetoryController@search_category');
Route::post('/search_customer', 'CustomerController@search_customer');
Route::post('/search_product', 'ProductController@search_product');

