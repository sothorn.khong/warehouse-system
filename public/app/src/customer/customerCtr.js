angular.module("warehouse");
warehouseApp.controller('customerCtr', function ($scope, $http, $filter, $sce, $location,$window,HttpClients,toastr) {
    $scope.formData = {};
    $scope.formDataSearch = {};
    $scope.listData = [];
    $scope.formUpdateData = {};
    $scope.id = "";

   /*pageination*/
    $scope.limit = "10";
    $scope.page = 1;
    $scope.total = 0;

    $scope.getListData = function(page){

        $scope.page = page;
        $scope.formDataSearch["page"] = page;
        $scope.formDataSearch["limit"] = $scope.limit;

        HttpClients.PostHttp('search_customer',$scope.formDataSearch).then(function(response){
            $scope.listData = response.data["data"];
            $scope.total = response.data["total"];
        });
    }

    $scope.getListData($scope.page);

    $scope.showLimit =function(limit){
        $scope.limit = limit;
        $scope.getListData(1);
    }

    $scope.actionSaveData = function(){
        HttpClients.PostHttp('api/customer',$scope.formData).then(function(response){
            if(response.data["success"]== true){
                $scope.formData = {};
                toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
                $scope.getListData(1);
            }else{
                toastr.warning(LANG["THIS_DATA_ALREADY_EXIT"], LANG["FAIL"]);
            }
           
        });
    }

    $scope.updateFormData = function(data){
        $scope.formUpdateData = data;
    }

    $scope.actionEditData = function(){
        HttpClients.PutHttp("api/customer",$scope.formUpdateData,$scope.formUpdateData["id"]).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListData($scope.page);
            $("#editForm").modal("hide");
        });
    }

    $scope.showFormDelete = function(id){
        $scope.id = id;
    }

    $scope.actionDeleteData = function(userId){
        HttpClients.DeleteHttp("api/customer",userId).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListData($scope.page);
            $("#deleteForm").modal("hide");
        });
    };
    
});
    

