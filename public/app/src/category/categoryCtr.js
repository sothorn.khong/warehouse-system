angular.module("warehouse");
warehouseApp.controller('categoryCtr', function ($scope, $http, $filter, $sce, $location,$window,HttpClients,toastr) {
    $scope.formData = {};
    $scope.formDataSearch = {};
    $scope.listData = [];
    $scope.formUpdateData = {};

    $scope.id = "";
    $scope.limit = "10";
    $scope.getListData = function(){
        $scope.formDataSearch["limit"] = $scope.limit;
        HttpClients.PostHttp('search_category',$scope.formDataSearch).then(function(response){
            $scope.listData = response.data["data"];
            //console.log(response.data);
        });
    }

    $scope.getListData();
    $scope.showLimit =function(limit){
        $scope.limit = limit;
        $scope.getListData();
    }

    $scope.actionSaveData = function(){
        HttpClients.PostHttp('api/category',$scope.formData).then(function(response){
            if(response.data["success"]== true){
                $scope.formData = {};
                toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
                $scope.getListData();
            }else{
                toastr.warning(LANG["THIS_DATA_ALREADY_EXIT"], LANG["FAIL"]);
            }
           
        });
    }
    
    $scope.updateFormData = function(data){
        $scope.formUpdateData = data;
    }

    $scope.actionEditData = function(){
        HttpClients.PutHttp("api/category",$scope.formUpdateData,$scope.formUpdateData["id"]).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListData();
            $("#editForm").modal("hide");
        });
    }

    $scope.showFormDelete = function(id){
        $scope.id = id;
    }

    $scope.actionDeleteData = function(userId){
        HttpClients.DeleteHttp("api/category",userId).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListData();
            $("#deleteForm").modal("hide");
        });
    }
});
    

