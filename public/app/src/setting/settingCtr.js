angular.module("warehouse");
warehouseApp.config(function (toastrConfig) {
    angular.extend(toastrConfig, {
        "closeButton": true,
        timeOut: 500,
    });
});
warehouseApp.controller('settingCtr', function ($scope, $http, $filter, $sce, $location,$window,HttpClients,toastr) {
   $scope.formData = [];
   

    $scope.saveSetting = function(){
        HttpClients.PostHttp('api/setting',$scope.formData).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getSetting();
        });
    }

    $scope.getSetting = function(){
        HttpClients.GetHttp('api/setting').then(function(response){
            $scope.formData = response.data["data"];
        });
    }

    $scope.getSetting();



});
    

