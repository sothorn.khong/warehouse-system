angular.module("warehouse");
warehouseApp.controller('userCtr', function ($scope, $http, $filter, $sce, $location,$window,HttpClients,toastr) {
    $scope.formData = {};
    $scope.formDataSearch = {};
    $scope.listUser = [];
    $scope.formUpdateUser = {};
    $scope.userId = "";
    /*pageination*/
    $scope.limit = "10";
    $scope.page = 1;
    $scope.total = 0;

    $scope.getListUser = function(page){
        $scope.page = page;
        $scope.formDataSearch["page"] = page;
        $scope.formDataSearch["limit"] = $scope.limit;
        HttpClients.PostHttp('search_user',$scope.formDataSearch).then(function(response){
            $scope.listUser = response.data["data"];
            $scope.total = response.data["total"];
            //console.log(response.data);
        });
    }

    $scope.showLimit =function(limit){
        $scope.limit = limit;
        $scope.getListUser(1);
    }


    $scope.getListUser($scope.page);

    $scope.saveUser = function(){
        HttpClients.PostHttp('api/users',$scope.formData).then(function(response){
            if(response.data["success"]== true){
                $scope.formData = {};
                toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
                $scope.getListUser(1);
            }else{
                toastr.warning(LANG["THIS_DATA_ALREADY_EXIT"], LANG["FAIL"]);
            }
           
        });
    }


    $scope.updateUser = function(data){
        $scope.formUpdateUser = data;
    }

    $scope.editUser = function(){
        HttpClients.PutHttp("api/users",$scope.formUpdateUser,$scope.formUpdateUser["id"]).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListUser($scope.page);
            $("#editUser").modal("hide");
        });
    }

    $scope.showFormDeleteUser = function(id){
        $scope.userId = id;
    }

    $scope.deleteUser = function(userId){
       
        HttpClients.DeleteHttp("api/users",userId).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListUser($scope.page);
            $("#deleteUser").modal("hide");
        });
    }

});
    

