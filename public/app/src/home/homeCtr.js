angular.module("warehouse");
warehouseApp.controller('homeCtr', function ($scope, $http, $filter, $sce, $location,$window,FileUploader,toastr,HttpClients) {
    $scope.formData = {};
    $scope.formDataSearch = {};
    $scope.listData = [];
    $scope.formUpdateData = {};
    $scope.id = "";

    var uploader = $scope.uploader = new FileUploader({
        url: 'api/ordering'
    });


    uploader.onBeforeUploadItem = function (item) {
        item.formData = [$scope.uploadData];
    };

    uploader.onSuccessItem = function () {
        toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
    };

    $scope.clearUpload = function () {
        uploader.clearQueue();
    };

   

    $scope.getListData = function(){
        HttpClients.PostHttp('search_brand',$scope.formDataSearch).then(function(response){
            $scope.listData = response.data["data"];
        });
    }

    $scope.getListData();

    $scope.actionSaveData = function(){
        HttpClients.PostHttp('api/brand',$scope.formData).then(function(response){
            if(response.data["success"]== true){
                $scope.formData = {};
                toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
                $scope.getListData();
            }else{
                toastr.warning(LANG["THIS_DATA_ALREADY_EXIT"], LANG["FAIL"]);
            }
           
        });
    }

    $scope.updateFormData = function(data){
        $scope.formUpdateData = data;
    }

    $scope.actionEditData = function(){
        HttpClients.PutHttp("api/brand",$scope.formUpdateData,$scope.formUpdateData["id"]).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListData();
            $("#editForm").modal("hide");
        });
    }

    $scope.showFormDelete = function(id){
        $scope.id = id;
    }

    $scope.actionDeleteData = function(userId){
        HttpClients.DeleteHttp("api/brand",userId).then(function(response){
            toastr.success(LANG["YOU_ARE_SUCCESSFULLY"], LANG["SUCCESS"]);
            $scope.getListData();
            $("#deleteForm").modal("hide");
        });
    };

});
    

