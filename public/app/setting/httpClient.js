angular.module("warehouse");


warehouseApp.service('HttpClients',function ($http) {

    this.GetHttp = function (url) {
        return  $http({
            method: "GET",
            url: url,
            headers: {"Content-Type": "application/json"},
        });
    };

    this.GetHttpById = function (url,Id) {
        return  $http({
            method: "GET",
            url: url+'/'+Id,
            headers: {"Content-Type": "application/json"},
        });
    };

    this.DeleteHttp = function (url,Id) {
        return  $http({
            method: "DELETE",
            url: url+'/'+Id,
            headers: {"Content-Type": "application/json"},
        });
    };

    this.PostHttp = function (url,datas) {
       return $http({
            method: "POST",
            url: url,
            data:datas,
            headers: {"Content-Type": "application/json"},
        });
    };

    this.PutHttp = function (url,datas,Id) {
      return $http({
        method: "PUT",
        url: url+'/'+Id,
        data:datas,
        headers: {"Content-Type": "application/json"},
      });
    };
});

