angular.module("warehouse");
warehouseApp.config(function ($routeProvider) {
    $routeProvider
        .when("/home", {
            templateUrl: "app/src/home/home.html",
            controller: "homeCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/home/homeCtr.js']
                    }]);
                }]
            }
        })

        .when("/setting", {
            templateUrl: "app/src/setting/setting.html",
            controller: "settingCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/setting/settingCtr.js']
                    }]);
                }]
            }
        })

        .when("/delivery", {
            templateUrl: "app/src/delivery/delivery.html",
            controller: "deliveryCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/delivery/deliveryCtr.js']
                    }]);
                }]
            }
        })

        .when("/brand", {
            templateUrl: "app/src/brand/brand.html",
            controller: "brandCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/brand/brandCtr.js']
                    }]);
                }]
            }
        })
        
        .when("/category", {
            templateUrl: "app/src/category/category.html",
            controller: "categoryCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/category/categoryCtr.js']
                    }]);
                }]
            }
        })
        .when("/customer", {
            templateUrl: "app/src/customer/customer.html",
            controller: "customerCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/customer/customerCtr.js']
                    }]);
                }]
            }
        })
        
        .when("/product", {
            templateUrl: "app/src/product/product.html",
            controller: "productCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/product/productCtr.js']
                    }]);
                }]
            }
        })
        .when("/user", {
            templateUrl: "app/src/user/user.html",
            controller: "userCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/user/userCtr.js']
                    }]);
                }]
            }
        })

        .when("/group", {
            templateUrl: "app/src/user_group/user_group.html",
            controller: "groupCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/user_group/user_groupCtr.js']
                    }]);
                }]
            }
        })

        .when("/warehouse", {
            templateUrl: "app/src/warehouse/warehouse.html",
            controller: "warehouseCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/warehouse/warehouseCtr.js']
                    }]);
                }]
            }
        })
        
        .when("/report", {
            templateUrl: "app/src/report/report.html",
            controller: "reportCtr",
            resolve: {
                lazy: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'warehouse',
                        files: ['app/src/report/reportCtr.js']
                    }]);
                }]
            }
        })
        
        .otherwise({
            redirectTo: '/home'
        });
});