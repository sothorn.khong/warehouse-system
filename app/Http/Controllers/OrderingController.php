<?php

namespace App\Http\Controllers;

use App\Ordering;
use Illuminate\Http\Request;

class OrderingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // require_once $_SERVER['DOCUMENT_ROOT'].'/third_party/PHPExcel.php';    
        // $excel = new PHPExcel(); 

        $uploadPath = $_SERVER['DOCUMENT_ROOT'] . '/app/files/';

       // $objPHPExcel = PHPExcel_IOFactory::load($uploadPath.'1.xlsx');


       

        $file_info = pathinfo($_FILES["file"]["name"]);
       
		$new_file_name =  "ordering.". $file_info["extension"];
        move_uploaded_file($_FILES["file"]["tmp_name"], $uploadPath . $new_file_name);



        // if (!file_exists($uploadPath)) {
        //     mkdir($uploadPath, 0777, true);
        // }

        // if (file_exists($uploadPath . $name . '.' . $extension)) {
        //     $increment = 1; //start with no suffix
        //     while (file_exists($uploadPath . $name . $increment . '.' . $extension)) {
        //         $increment++;
        //     }
        // } else {
        //     $increment = '';
        // }



        //$basename = 'ordering.' . $extension;
       // move_uploaded_file($tempPath, $uploadPath . $basename);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ordering  $ordering
     * @return \Illuminate\Http\Response
     */
    public function show(Ordering $ordering)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ordering  $ordering
     * @return \Illuminate\Http\Response
     */
    public function edit(Ordering $ordering)
    {

       

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ordering  $ordering
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ordering $ordering)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ordering  $ordering
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ordering $ordering)
    {
        //
    }
}
