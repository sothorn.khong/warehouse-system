<?php

namespace App\Http\Controllers;
use App\User;
use App\General;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function search_user(Request $request)
    {
        $user = General::SearchUser();
        return response()->json($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {  
        $params = json_decode(file_get_contents("php://input"));
        $data = array();
        $checkUser = General::CheckExitUser();
        if($checkUser == 1){
            $data["success"] = false;
        }else{
            User::create([
                'name' => $params->name,
                'email' => $params->email,
                'password' => bcrypt($params->password),
            ]);
            $data["success"] = true;
        }


        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        $params = json_decode(file_get_contents("php://input"));
        $user->name =  $params->name;
        if(isset($params->new_password)){
            $user->password =  bcrypt($params->new_password);
        }
        $user->save();
        return response()->json([
            "success"=>true
       ]);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json([
            "success"=>true
       ]);  
    }
}
