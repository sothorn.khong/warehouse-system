<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\General;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $params = json_decode(file_get_contents("php://input"));
        $data = array();
        $name = isset($params->name) ? $params->name: "";
        $phone = isset($params->phone) ?  $params->phone: "";
        $address = isset($params->address) ?  $params->address: "";
        $email = isset($params->email) ?  $params->email: "";
        $barcode = isset($params->barcode) ?  $params->barcode: "";

        
        Customer::create([
            "barcode"=>$barcode,
            "name" => $name,
            "phone"=>$phone,
            "email"=>$email,
            "address"=>$address
        ]);
        $data["success"] = true;
        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
        $params = json_decode(file_get_contents("php://input"));

        $name = isset($params->name) ? $params->name: "";
        $phone = isset($params->phone) ?  $params->phone: "";
        $address = isset($params->address) ?  $params->address: "";
        $email = isset($params->email) ?  $params->email: "";
        
        $customer->name =  $name;
        $customer->phone =  $phone;
        $customer->address =  $address;
        $customer->email =  $email;
        $customer->save();
        return response()->json([
            "success"=>true
       ]);  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Customer::find($id);
        $delete->delete();
        return response()->json([
            "success"=>true
       ]);  
    }

    public function search_customer(){
        $data = General::SearchCustomer();
        return response()->json($data);
    }
}
