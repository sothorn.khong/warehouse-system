<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\General;
class CagetoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = json_decode(file_get_contents("php://input"));
        $data = array();
        $checkExit = General::CheckExitCategory();
        if($checkExit == 1){
            $data["success"] = false;
        }else{

            $name = isset($params->name) ? $params->name: "";
            $description = isset($params->description) ?  $params->description: "";
            
            Category::create([
                "name" => $name,
                "description"=>$description
            ]);
            $data["success"] = true;
        
        }
        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $params = json_decode(file_get_contents("php://input"));
        $name = isset($params->name) ? $params->name: "";
        $description = isset($params->description) ?  $params->description: "";
        
        $category->name =  $name;
        $category->description =  $description;
        $category->save();
        return response()->json([
            "success"=>true
       ]);  


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Category::find($id);
        $delete->delete();
        return response()->json([
            "success"=>true
       ]);  
        //
    }

    public function search_category(){
        $data = General::SearchCategory();
        return response()->json([
             "data"=>$data
        ]);
    }
}
