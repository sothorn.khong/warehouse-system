<?php

namespace App\Http\Controllers;

use App\Product;
use App\General;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = json_decode(file_get_contents("php://input"));
        $data = array();
        $name = isset($params->name) ? $params->name: "";
        $cat_id = isset($params->cat_id) ?  $params->cat_id: "";
        $brand_id = isset($params->brand_id) ?  $params->brand_id: "";
        $object = General::GetLastProductID();
        if($object == 1){
            $barcode = $object;
        }else{
            $barcode = $object + 1;
        }
        
        $barcode_lenght= str_pad($barcode,14,'0',STR_PAD_LEFT);
        Product::create([
            "barcode"=>$barcode_lenght,
            "name" => $name,
            "brand_id"=>$brand_id,
            "cat_id"=>$cat_id
            
        ]);

        $data["success"] = true;
        return response()->json($data);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $params = json_decode(file_get_contents("php://input"));
        $data = array();
        $name = isset($params->name) ? $params->name: "";
        $cat_id = isset($params->cat_id) ? (int) $params->cat_id: "";
        $brand_id = isset($params->brand_id) ? (int)  $params->brand_id: "";
        $is_active = isset($params->is_active) ?  (int)$params->is_active: "";

        $product->name = $name;
        $product->cat_id = $cat_id;
        $product->brand_id = $brand_id;
        $product->is_active = $is_active;
        $product->save();
        return response()->json([
            "success"=>true
       ]);  

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Product::find($id);
        $delete->delete();
        return response()->json([
            "success"=>true
       ]);  
        //
    }

    public function search_product(){
        $data = General::SearchProduct();
        return response()->json($data);
    }
}
