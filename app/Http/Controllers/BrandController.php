<?php

namespace App\Http\Controllers;

use App\Brand;
use App\General;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = json_decode(file_get_contents("php://input"));
        $data = array();
        $checkExit = General::CheckExitBrand();
        if($checkExit == 1){
            $data["success"] = false;
        }else{

            $name = isset($params->name) ? $params->name: "";
            $description = isset($params->description) ?  $params->description: "";
            

            Brand::create(array(
                "name" => $name,
                "description"=>$description
                
            ));
            $data["success"] = true;

        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        //

        $params = json_decode(file_get_contents("php://input"));
        $name = isset($params->name) ? $params->name: "";
        $description = isset($params->description) ?  $params->description: "";
        
        $brand->name =  $name;
        $brand->description =  $description;
        $brand->save();
        return response()->json([
            "success"=>true
       ]);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Brand::find($id);
        $delete->delete();
        return response()->json([
            "success"=>true
       ]);  
    }
    /** 
    * get list brand
    * @param $page,$limit
    */
    public function search_brand(Request $request){
        $data = General::SearchBrand();
        return response()->json([
             "data"=>$data
        ]);
    }
}
