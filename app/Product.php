<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $table = 'products';
    
    protected $fillable = [
        'barcode','name','brand_id','cat_id',
    ];
}
