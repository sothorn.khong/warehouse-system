<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Setting extends Model
{
    //update all data in table setting 
    public function scopeUpdateAllSetting(){
        $data = array();
        $FORM_DATA = json_decode(file_get_contents("php://input"));
        foreach( $FORM_DATA as $settingValue){
            DB::table('settings')
            ->where('id', $settingValue->id)
            ->update(['value' => $settingValue->value]);
        }

        $data["success"] = true;
        return $data;
    }
}
