<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class General extends Model
{
    //

    protected $params;

    public function __construct()
    {
        $this->params = json_decode(file_get_contents("php://input"));

    }

     /*Get List Product */

    public function scopeSearchProduct(){
        $data = array();
        $sub_data = array();
        $objectProduct = $this->sqlSearchProduct();
        $page = isset($this->params->page) ? $this->params->page : 1;
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;

        foreach( $objectProduct as $key=>$value){
            $sub_data["index"] = ($page - 1) * $limit + ($key +1);
            $sub_data["id"] = $value->id;
            $sub_data["barcode"] = $value->barcode;
            $sub_data["name"] = $value->name;
            $sub_data["brand_name"] = $value->brand_name;
            $sub_data["category_name"] = $value->category_name;
            $sub_data["brand_id"] = (string) $value->brand_id;
            $sub_data["cat_id"] = (string) $value->cat_id;
            $sub_data["is_active"] = (string)$value->is_active;
            $sub_data["created_at"] = $value->created_at;
            $sub_data["updated_at"] = $value->updated_at;
            $data[] = $sub_data;
        }

        $countProduct = $this->countSearchProduct();
        $total = $countProduct > $limit ? $countProduct : 0;
        return array("data"=>$data,"total"=>$total);
    }

    public function sqlSearchProduct(){
        $name = isset($this->params->name) ? $this->params->name : "";
        $page = isset($this->params->page) ? $this->params->page : 1;
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;
        $offset = ($page - 1) * $limit;
        $data = DB::table('products')
        ->select("products.*","brands.name as brand_name","categories.name as category_name")
        ->when($name, function ($query) use ($name) {
             return $query->where('barcode', 'like','%'.$name.'%');
         })  
        ->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        }) 
        ->when($offset, function ($query) use ($offset) {
            return $query->offset($offset);
        }) 
        ->leftJoin('brands','brands.id',"=","products.brand_id")
        ->leftJoin('categories','categories.id',"=","products.cat_id")
        ->get();
        return $data;
    }

    public function countSearchProduct(){
        $name = isset($this->params->name) ? $this->params->name : "";
        $data = DB::table('products')
        ->select("products.*","brands.name as brand_name","categories.name as category_name")
        ->when($name, function ($query) use ($name) {
             return $query->where('barcode', 'like','%'.$name.'%');
         })  
        ->leftJoin('brands','brands.id',"=","products.brand_id")
        ->leftJoin('categories','categories.id',"=","products.cat_id")
        ->count();
        return $data;
    }

    /*End Code Get List Product */

    /*Get List User */

    public function  scopeSearchUser(){
        $page = isset($this->params->page) ? $this->params->page : 1;
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;
        $objectUser = $this->sqlSearchUser();
        $data = array();
        $sub_data = array();
        foreach($objectUser as $key=>$value){
            $sub_data["index"] = ($page - 1) * $limit + ($key +1);
            $sub_data["id"] = $value->id;
            $sub_data["name"] = $value->name;
            $sub_data["email"] = $value->email;
            $sub_data["password"] = $value->password;
            $sub_data["created_at"] = $value->created_at;
            $sub_data["updated_at"] = $value->updated_at;
            $data[] = $sub_data;
        }
        $countUser = $this->countAllUser();
        $totalUser = ($countUser>$limit) ? $countUser  : 0;
        return array("data"=>$data,"total"=>$totalUser);
    }

    public function sqlSearchUser(){
        $page = isset($this->params->page) ? $this->params->page : 1;
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;
        $offset = ($page - 1) * $limit;
        $name = isset($this->params->name) ? $this->params->name : "";
        $data = DB::table('users')
        ->when($name, function ($query) use ($name) {
            return $query->where('name', 'like','%'.$name.'%');
        })
        ->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        }) 
        ->when($offset, function ($query) use ($offset) {
            return $query->offset($offset);
        })   
        ->get();
        return $data;
    }

    public function countAllUser(){
        $name = isset($this->params->name) ? $this->params->name : "";
        $data = DB::table('users')
        ->when($name, function ($query) use ($name) {
            return $query->where('name', 'like','%'.$name.'%');
        })  
        ->count();
        return $data;
    }

    /*End Code Get List User */

    /* Get list customer*/
    public function  scopeSearchCustomer(){
        $page = isset($this->params->page) ? $this->params->page : 1;
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;
        $objectUser = $this->sqlSearchCustomer();
        $data = array();
        $sub_data = array();
        foreach($objectUser as $key=>$value){
            $sub_data["index"] = ($page - 1) * $limit + ($key +1);
            $sub_data["id"] = $value->id;
            $sub_data["barcode"] = $value->barcode;
            $sub_data["name"] = $value->name;
            $sub_data["phone"] = $value->phone;
            $sub_data["email"] = $value->email;
            $sub_data["address"] = $value->address;
            $sub_data["created_at"] = $value->created_at;
            $sub_data["updated_at"] = $value->updated_at;
            $data[] = $sub_data;
        }
        $countUser = $this->countAllUser();
        $totalUser = ($countUser>$limit) ? $countUser  : 0;
        return array("data"=>$data,"total"=>$totalUser);
    }

    public function sqlSearchCustomer(){
        $page = isset($this->params->page) ? $this->params->page : 1;
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;
        $offset = ($page - 1) * $limit;
        $name = isset($this->params->name) ? $this->params->name : "";
        $data = DB::table('customers')
        ->when($name, function ($query) use ($name) {
            return $query->where('phone', $name);
        })
        ->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        }) 
        ->when($offset, function ($query) use ($offset) {
            return $query->offset($offset);
        })
        ->orderBy('id', 'DESC')
        ->get();
        return $data;
    }

    public function countAllCustomer(){
        $name = isset($this->params->name) ? $this->params->name : "";
        $data = DB::table('customers')
        ->when($name, function ($query) use ($name) {
            return $query->where('phone', $name);
        })  
        ->count();
        return $data;
    }

    /* End code get list customer*/

    public function scopeSearchBrand(){
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;
       $data = DB::table('brands')
        ->when($limit, function ($query) use ($limit) {
        return $query->limit($limit);
        }) 
        ->get();
       return $data;
    }

    public function scopeSearchCategory(){
        $limit = isset($this->params->limit) ? (int)$this->params->limit : 20;

        $data = DB::table('categories')
        ->when($limit, function ($query) use ($limit) {
            return $query->limit($limit);
        }) 
         ->get();
        return $data;
     }
    



    public function scopeGetLastProductID(){
        $data = DB::table('products')
        ->limit(1)
        ->orderBy('id', 'DESC')
        ->first();
        if($data){
            return $data->id;  
        }else{
            return 1; 
        }
    } 

    public function scopeCheckExitUser(){
        $data = DB::table('users')
            ->where("email",$this->params->email) 
            ->limit(1)
            ->count();
        if($data){
            return 1;
        }else{
            return 2;
        }
    }

    public function scopeCheckExitBrand(){

        $data = DB::table('brands')
        ->where("name",$this->params->name) 
        ->limit(1)
        ->count();
        if($data){
            return 1;
        }else{
            return 2;
        }
    }

    public function scopeCheckExitCategory(){
        $data = DB::table('categories')
        ->where("name",$this->params->name) 
        ->limit(1)
        ->count();
        if($data){
            return 1;
        }else{
            return 2;
        }
    }
}
